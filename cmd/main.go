package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

type Hospital struct {
	Name     string
	Staffs   []*Staff
	Patients []*Patient
	Adresses []*Address
}
type HospitalResponse struct {
	Id   int64
	Name string
}

type Address struct {
	HospitalId int64
	Region     string
	Street     string
}
type AddressRes struct {
	Id     int64
	Region string
}

type Staff struct {
	HospitalID  int64
	FullName    string
	PhoneNumber string
}
type StaffRes struct {
	Id       int64
	FullName string
}

type Patient struct {
	HospitalId  int64
	FullName    string
	PatientInfo string
	PhoneNumber string
}
type PatientRes struct {
	Id       int64
	FullName string
}

const (
	PostgresHost     = "localhost"
	PostgresPort     = 5432
	PostgresUser     = "developer"
	PostgresPassword = "2002"
	PostgresDatabase = "hospital"
)

func main() {
	connDB := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		PostgresHost, PostgresPort, PostgresUser, PostgresPassword, PostgresDatabase,
	)
	db, err := sql.Open("postgres", connDB)
	if err != nil {
		log.Println("Error while connecting postgres", err)
	}
	defer db.Close()
	// CreateInfo(db)
	UpdateInfo(db)
}

// func CreateInfo(db *sql.DB) {
// 	info := &Hospital{
// 		Name: "Medical",
// 		Staffs: []*Staff{
// 			{
// 				FullName:    "Bobir Davlatov",
// 				PhoneNumber: "+998908907867",
// 			},
// 			{
// 				FullName:    "Komil Sattorov",
// 				PhoneNumber: "+998988765432",
// 			},
// 		},
// 		Patients: []*Patient{
// 			{
// 				FullName:    "Lobarxon",
// 				PhoneNumber: "+998994432345",
// 				PatientInfo: "Bu bemor hozirgi holati o'rta",
// 			},
// 			{
// 				FullName:    "Gavhar Ismoilova",
// 				PhoneNumber: "+998995455656",
// 				PatientInfo: "Bu bemor hozirgi holati yaxshi",
// 			},
// 		},
// 		Adresses: []*Address{
// 			{
// 				Region: "Chilonzor",
// 				Street: "Qatortol 68",
// 			},
// 			{
// 				Region: "Yunusobod",
// 				Street: "Amir Temur 78",
// 			},
// 		},
// 	}
// 	tr, err := db.Begin()
// 	if err != nil {
// 		log.Println("Error begin transaction", err)
// 	}

// 	query1 := `
// 	INSERT INTO
// 		hospital(name)
// 	VALUES
// 		($1)
// 	RETURNING
// 		id, name
// 	`
// 	var response HospitalResponse
// 	err = tr.QueryRow(query1, info.Name).Scan(&response.Id, &response.Name)
// 	if err != nil {
// 		tr.Rollback()
// 		log.Println("Error while inserting hospital info", err)
// 	}
// 	fmt.Println(response)

// 	res1 := StaffRes{}
// 	for _, staff := range info.Staffs {
// 		query2 := `
// 		INSERT INTO
// 			staff(hospital_id, full_name, phone_number)
// 		VALUES
// 			($1, $2, $3)
// 		RETURNING
// 			id,	full_name
// 		`
// 		err = tr.QueryRow(query2, response.Id, staff.FullName, staff.PhoneNumber).Scan(&res1.Id, &res1.FullName)
// 		if err != nil {
// 			tr.Rollback()
// 			fmt.Println("Error while inserting staff info: ", err)
// 		}
// 	}
// 	fmt.Println("Staff Info: ", res1)

// 	res2 := PatientRes{}
// 	for _, patient := range info.Patients {
// 		query3 := `
// 		INSERT INTO
// 			patients(hospital_id, full_name, patient_info, phone_number)
// 		VALUES
// 			($1, $2, $3, $4)
// 		RETURNING
// 			id,
// 			full_name
// 		`
// 		err = tr.QueryRow(query3, response.Id, patient.FullName, patient.PatientInfo, patient.PhoneNumber).
// 			Scan(&res2.Id, &res2.FullName)
// 		if err != nil {
// 			tr.Rollback()
// 			fmt.Println("Error while inserting patients info: ", err)
// 		}
// 	}
// 	fmt.Println("Patient Info: ", res2)

// 	res3 := AddressRes{}
// 	for _, address := range info.Adresses {
// 		query4 := `
// 		INSERT INTO
// 			addresses(hospital_id, region, street)
// 		VALUES
// 			($1, $2, $3)
// 		RETURNING
// 			id,
// 			region`
// 		err = tr.QueryRow(query4, response.Id, address.Region, address.Street).
// 			Scan(&res3.Id, &res3.Region)
// 		if err != nil {
// 			tr.Rollback()
// 			log.Println("Error while create address info: ", err)
// 		}

// 	}
// 	fmt.Println("Address Info:", res3)
// }

func UpdateInfo(db *sql.DB) {
	var id int64
	print("Enter hospital id: ")
	fmt.Scan(&id)
	hospital := &HospitalResponse{Id: id, Name: "Update hospital name"}
	tx, err := db.Begin()
	if err != nil {
		fmt.Println("Error begin transaction: ", err)
		return
	}
	res := HospitalResponse{}
	err = tx.QueryRow(`
	UPDATE
		hospital
	SET 
		name=$1
	WHERE
		id=$2
	RETURNING 
		id, name
	`, hospital.Name, hospital.Id).Scan(&res.Id, &res.Name)
	if err != nil {
		tx.Rollback()
		log.Println("Error update hospital info", err)
	}
	err = tx.Commit()
	if err != nil {
		fmt.Println("Error commit tr: ", err)
	}
}
